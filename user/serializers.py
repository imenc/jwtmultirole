from rest_framework.serializers import ModelSerializer
from django.contrib.auth.models import User, Group


class UserSerializer(ModelSerializer):
    # group_name = serializers.CharField(source='groups__name')
    class Meta:
        fields = ('id', 'username',  'first_name', 'last_name',  'password', 'groups', 'email')
        model = User
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.is_staff = True
        user.save()

        return user


class GroupCustomSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')


class UserCustomSerializer(ModelSerializer):
    groups = GroupCustomSerializer(many=True)
    class Meta:
        model = User
        fields = ('username', 'email', 'is_staff', 'groups',)