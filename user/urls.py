from django.conf.urls import include, url
from django.urls import path

from user import views


urlpatterns = [
    url(r'^users/?$', views.UserListView.as_view()),
    path('token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),    # + simple jwt custom claims
    path('hello/', views.HelloView.as_view(), name='hello'),
    path('user_custom/', views.UserCustomListView.as_view(), name='user_custom'),
]