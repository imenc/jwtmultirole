from django.http import request
from rest_framework import status                               # + simple jwt custom claims
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet, ModelViewSet
from rest_framework.permissions import AllowAny

from user.permissions import IsAdminUser, IsLoggedInUserOrSuperAdmin, IsAdminOrAnonymousUser, IsBackOfficeUser
from rest_framework import generics, mixins, status, filters, viewsets
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer      # + simple jwt custom claims
from rest_framework_simplejwt.views import TokenObtainPairView                  # + simple jwt custom claims
from rest_framework_simplejwt.settings import api_settings                      # + simple jwt custom claims

# from user.models import User                                                  # - deactivate custom user
from django.contrib.auth.models import User                                     # + simple jwt custom claims
from user.serializers import UserSerializer, GroupCustomSerializer, UserCustomSerializer

from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from user.permissions import IsAdminUser


# + simple jwt custom claims
class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        qs_role_names = user.groups.values_list('name', flat=True)
        # qs_role_names = user.groups.values('id', 'name')
        role_name = list(qs_role_names)  # QuerySet to `list`

        # Add custom claims
        token['user_name'] = user.username
        token['role_name'] = role_name

        return token


# + simple jwt custom claims
class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = [TokenAuthentication]
    # permission_classes = (HasGroupPermission, )
    # permission_groups = {
    #     'create': ['admin'],
    #     'list': ['admin', 'anonymous'],
    #     'retrieve': ['admin', 'anonymous'],
    #     'update': ['admin', 'anonymous'],
    #     'destroy': ['admin']
    # }

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [IsAdminUser]
        elif self.action == 'list':
            permission_classes = [IsAdminOrAnonymousUser]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUserOrSuperAdmin]
        elif self.action == 'destroy':
            permission_classes = [IsLoggedInUserOrSuperAdmin]
        return [permission() for permission in permission_classes]


# APIView defined for UserView
# class UserView(APIView):
#     authentication_classes = [TokenAuthentication]
#     permission_classes = [APIViewPermission]
#     required_groups = {
#         'GET': ['anonymous'],
#         'POST': ['admin'],
#         'PUT': ['__all__']
#     }
#
#     def get(self, request):
#         user = User.objects.all()
#         return Response({"users": user})


class LoginView(ViewSet):
    serializer_class = AuthTokenSerializer

    @staticmethod
    def create(request):
        return ObtainAuthToken().post(request)


class LogoutView(APIView):
    def get(self, request, format=None):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


class UserListView(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get_queryset(self):
        queryset = User.objects.all()
        # queryset = User.objects.select_related('groups').values('id', 'username', 'first_name', 'last_name', 'groups')
        return queryset


class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        content = {'message': 'Hello, World!'}
        return Response(content)


class UserCustomListView(generics.ListCreateAPIView):
    serializer_class = UserCustomSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)

    def get_queryset(self):
        queryset = User.objects.all()
        return queryset
